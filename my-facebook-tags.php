<?php 


/*

Plugin Name: Tom's Facbook Tags
Plugin URI: http://tomhaxell.com
Description: This plugin adds some Facebook Open Graph tags to our single posts.
Version: 1.0.0
Author: Tom Haxell
License: GPL2
*/


add_action('wp_head', 'my_facebook_tags' );
function my_facebook_tags() {
	if( is_single() ) {
		?>
	<meta property="og:title" content="<?php the_title() ?>" />
	<meta property="og:site_name" content="<?php bloginfo('name') ?>" />
	<meta property="og:url" content="<?php the_permalink() ?>" />
	<meta property="og:description" content="<?php the_excerpt() ?>" />
	<meta property="og:type" content="article" />

	<?php 
		if ( has_post_thumbnail() ) :
			$image= wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
	?>

	<meta property="og:image" content="<?php echo $image[0]; ?>"/>
	<?php endif; ?>

	<?php
	}
}


add_filter('login_errors', 'login_error_message');

function login_error_message($error) {
	$error = "Damn, that ain't the password";
	return $error;
}



add_action('admin_menu', 'my_plugin_menu');

function my_plugin_menu() {
	add_menu_page('My Plugin Settings', 'Plugin Settings', 'administrator', 'my-plugin-settings', 'my_plugin_settings_page', 'dashicons-admin-generic');
}

function my_plugin_settings_page() {
	//
}

add_action('admin_init', 'my_plugin_settings');

function my_plugin_settings() {
	register_setting( 'my-plugin-settings-group','accountant_name' );
	register_setting('my-plugin-settings-group', 'accountant_phone' );
	register_setting( 'my-plugin-settings-group', 'accountant_email' );
}























